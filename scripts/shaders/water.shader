shader_type spatial;

void vertex() {
	VERTEX.y += sin(TIME * 0.8 + VERTEX.x + VERTEX.z) / 30.0;
}

void fragment() {
	//NORMAL = normalize(cross(dFdx(VERTEX), dFdy(VERTEX)));
	ALBEDO = vec3(83.0 / 255.0, 211.0 / 255.0, 239.0 / 255.0);
}