shader_type canvas_item;

void fragment() {
	COLOR = texture(SCREEN_TEXTURE, SCREEN_UV);
	COLOR = COLOR + vec4(0.1, 0.1, 0.1, 0.005);
}