extends RigidBody
# This object is a RigidBody because it needs it for rotation.
# Since it must be static, its gravity_scale is set to 0.

# velocity used for the garden rotation
var drag_ang_vel = 0

onready var first_spot = $Spots/First
onready var second_spot = $Spots/Second
onready var third_spot = $Spots/Third
onready var fourth_spot = $Spots/Fourth
onready var UI = get_parent().get_node("GameUI")


func _ready():
	# place statue
	if Globals.current_statue != null:
		place_statue_on_fountain(Globals.current_statue)


func _physics_process(delta):
	if get_node("/root/Game/GameUI").has_node("MainMenu"):
		drag_ang_vel = 0.2
	# set garden rotation
	angular_velocity.y = drag_ang_vel
	if drag_ang_vel > 0:
		drag_ang_vel = max(drag_ang_vel - 0.1, 0)
	elif drag_ang_vel < 0:
		drag_ang_vel = min(drag_ang_vel + 0.1, 0)


func _input(event):
	# garden rotation with screen drag
	if event is InputEventScreenDrag:
		drag_ang_vel = clamp(event.relative.x, -5, 5)


func place_statue_on_fountain(statue_name):
	var fountain_top = $Fountain/Top
	var model_scene = load("res://assets/models/decorations/" + statue_name.to_lower() + \
					"/" + statue_name.capitalize() + ".tscn")
	var model
	if model_scene != null:
		model = model_scene.instance()
	else:
		# placeholder for debugging
		model = MeshInstance.new()
		model.mesh = PrismMesh.new()
	model.set_name("Statue")
	for i in fountain_top.get_children():
		if i.name.find("Statue") != -1:
			i.queue_free()
	fountain_top.add_child(model)
	Globals.current_statue = statue_name


func _on_FirstSpot_input_event( camera, event, click_position, click_normal, shape_idx ):
	if event is InputEventScreenTouch and event.is_pressed():
		Globals.play_sound("click")
		var plant = null
		for i in first_spot.get_children():
			if i.get_name().find("Plant") != -1:
				plant = i
		if plant == null:
			UI.show_spot_creation(first_spot)
		else:
			UI.show_plant_menu(plant, first_spot)


func _on_SecondSpot_input_event( camera, event, click_position, click_normal, shape_idx ):
	if event is InputEventScreenTouch and event.is_pressed():
		Globals.play_sound("click")
		var plant = null
		for i in second_spot.get_children():
			if i.get_name().find("Plant") != -1:
				plant = i
		if plant == null:
			UI.show_spot_creation(second_spot)
		else:
			UI.show_plant_menu(plant, second_spot)


func _on_ThirdSpot_input_event( camera, event, click_position, click_normal, shape_idx ):
	if event is InputEventScreenTouch and event.is_pressed():
		Globals.play_sound("click")
		var plant = null
		for i in third_spot.get_children():
			if i.get_name().find("Plant") != -1:
				plant = i
		if plant == null:
			UI.show_spot_creation(third_spot)
		else:
			UI.show_plant_menu(plant, third_spot)


func _on_FourthSpot_input_event( camera, event, click_position, click_normal, shape_idx ):
	if event is InputEventScreenTouch and event.is_pressed():
		Globals.play_sound("click")
		var plant = null
		for i in fourth_spot.get_children():
			if i.get_name().find("Plant") != -1:
				plant = i
		if plant == null:
			UI.show_spot_creation(fourth_spot)
		else:
			UI.show_plant_menu(plant, fourth_spot)


func _on_FountainArea_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventScreenTouch and event.is_pressed():
		Globals.play_sound("click")
		UI.show_statue_selection_menu()



func _on_WorldEnvironment_night_finished():
	$Tween.interpolate_property($Fountain/OmniLight, "light_energy", 1.5, 0.0, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()


func _on_WorldEnvironment_night_started():
	$Tween.interpolate_property($Fountain/OmniLight, "light_energy", 0.0, 1.5, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
