extends Spatial

# amount of exp points needed for each plant level
const EXP_POINTS_BY_LEVEL = {1:0, 2:150, 3:165, 4:180, 5:200, 6:220, 7:240, 8:270, 9:290, 10:320}
# production for each level
const PROD_BY_LEVEL = {1:25, 2:30, 3:35, 4:40, 5:45, 6:50, 7:55, 8:60, 9:70, 10:75}
# Timers max value
const PRODUCTION_TIME = 3600
const WATER_NORMAL_TIME = 8 * 3600
const WATER_CRITIC_TIME = 22 * 3600
const COMPOST_NORMAL_TIME = 10 * 3600
const COMPOST_CRITIC_TIME = 38 * 3600

var plant_id
var phase
var species
var nickname setget _set_nickname
var level
var exp_points
var production
var alive
var current_pot
var spot_index

onready var prod_timer = $ProductionTimer
onready var water_timer_normal = $WaterTimers/Normal
onready var water_timer_critic = $WaterTimers/Critic
onready var compost_timer_normal = $CompostTimers/Normal
onready var compost_timer_critic = $CompostTimers/Critic
onready var timers = [prod_timer, water_timer_normal, water_timer_critic,
				compost_timer_normal, compost_timer_critic]

signal exp_points_changed
signal nickname_changed
signal has_died


func _ready():
	self.scale = Vector3(0.15, 0.15, 0.15)
	_setup_timers()


func _physics_process(delta):
	for timer in timers:
		# grandparent node is Spots when the plant is active
		timer.paused = not get_parent().get_parent().name == "Spots"
	if get_parent().name in ["First", "Second", "Third", "Fourth"]:
		spot_index = get_parent().name
	else:
		spot_index = null


func init(species):
	self.plant_id = Globals.get_new_plant_id()
	set_name("Plant" + str(plant_id))
	self.phase = 1
	self.species = species
	self.nickname = species.capitalize()
	self.level = 1
	self.exp_points = 0
	self.production = PROD_BY_LEVEL[1]
	self.alive = true
	self.spot_index = null
	_load_model_by_phase()
	current_pot = "DEFAULTPOT"
	change_pot(current_pot)
	_place_plant_on_top()


func add_experience(value):
	if level >= 10:
		return # 10 is the max level
	exp_points += value
	if exp_points >= EXP_POINTS_BY_LEVEL[level + 1]:
		# new level
		var remainder = value - (EXP_POINTS_BY_LEVEL[level + 1] - (exp_points - value))
		exp_points = remainder
		level += 1
		_update_production_value()
		_update_phase_level()
		_unlock_new_item()
		Globals.play_sound("level_up")
	emit_signal("exp_points_changed")


func apply_water():
	# refill the water timers
	water_timer_normal.wait_time = WATER_NORMAL_TIME
	water_timer_normal.start()
	water_timer_critic.wait_time = WATER_CRITIC_TIME
	water_timer_critic.stop()


func apply_compost(compost):
	var secs_to_fill
	if compost_timer_normal.time_left > 0:
		secs_to_fill = COMPOST_NORMAL_TIME * compost.percentage
	elif compost_timer_critic.time_left > 0:
		# restart critic timer
		_reset_timer(compost_timer_critic, false)
		secs_to_fill = COMPOST_NORMAL_TIME * compost.percentage / 2
	var current = min(COMPOST_NORMAL_TIME, compost_timer_normal.time_left + secs_to_fill)
	var secs_before_compost = compost_timer_normal.time_left
	compost_timer_normal.wait_time = current
	compost_timer_normal.start()
	# increase experience
	var secs_filled = COMPOST_NORMAL_TIME - secs_before_compost
	add_experience(int(ceil(secs_filled * compost.percentage / 200)))
	# decrease compost amount
	compost.amount -= 1


func get_timer_time_left(timer_name):
	var time_left
	match timer_name:
		"WaterNormal":
			time_left = water_timer_normal.time_left
		"WaterCritic":
			if water_timer_normal.time_left > 0:
				time_left = WATER_CRITIC_TIME
			else:
				time_left = water_timer_critic.time_left
		"CompostNormal":
			time_left = compost_timer_normal.time_left
		"CompostCritic":
			if compost_timer_normal.time_left > 0:
				time_left = COMPOST_CRITIC_TIME
			else:
				time_left = compost_timer_critic.time_left
	return time_left


func save_plant():
	var save_dict = {
		"plant_id":plant_id,
		"phase":phase,
		"species":species,
		"nickname":nickname,
		"level":level,
		"exp_points":exp_points,
		"production":production,
		"alive":alive,
		"current_pot":current_pot,
		"spot_index":spot_index
	}
	return save_dict


func load_plant(dict):
	set_name("Plant" + str(dict["plant_id"]))
	plant_id = int(dict["plant_id"])
	phase = int(dict["phase"])
	species = dict["species"]
	level = int(dict["level"])
	nickname = dict["nickname"]
	exp_points = int(dict["exp_points"])
	production = int(dict["production"])
	alive = dict["alive"]
	current_pot = dict["current_pot"]
	spot_index = dict["spot_index"]
	_load_model_by_phase()
	change_pot(current_pot)
	_place_plant_on_top()
	if spot_index != null:
		var spots = get_node("/root/Game/Garden/Spots")
		get_parent().remove_child(self)
		spots.get_node(spot_index).add_child(self)


func save_timers():
	var w_critic_t
	var c_critic_t
	if water_timer_critic.time_left == 0:
		w_critic_t = WATER_CRITIC_TIME
	else:
		w_critic_t = water_timer_critic.time_left
	if compost_timer_critic.time_left == 0:
		c_critic_t = COMPOST_CRITIC_TIME
	else:
		c_critic_t = compost_timer_critic.time_left
	var save_dict = {
		"plant_id":plant_id,
		"sys_secs_at_close":OS.get_system_time_secs(),
		"active":get_parent().get_parent().name == "Spots",
		"production":prod_timer.time_left,
		"water_normal":water_timer_normal.time_left,
		"water_critic":w_critic_t,
		"compost_normal":compost_timer_normal.time_left,
		"compost_critic":c_critic_t
	}
	return save_dict


func load_timers(dict):
	var elapsed_time = 0
	if dict["active"]:
		elapsed_time = OS.get_system_time_secs() - dict["sys_secs_at_close"]
	## PRODUCTION ##
	if dict["production"] > elapsed_time:
		prod_timer.wait_time = dict["production"] - elapsed_time
	else:
		var s_to_count = elapsed_time - dict["production"]
		var rem = fmod(s_to_count, PRODUCTION_TIME)
		s_to_count -= rem
		var h = int(s_to_count / PRODUCTION_TIME)
		if alive:
			Globals.sollars += production * h
		prod_timer.wait_time = PRODUCTION_TIME - rem
	prod_timer.start()
	## WATER ##
	var w_diff = dict["water_normal"] - elapsed_time
	if w_diff > 0:
		water_timer_normal.wait_time = w_diff
		water_timer_normal.start()
	else:
		water_timer_normal.stop()
	# 0.1 to give it time to send the has_died signal
	if water_timer_normal.is_stopped():
		water_timer_critic.wait_time = max(dict["water_critic"] - (elapsed_time - dict["water_normal"]), 0.1)
		water_timer_critic.start()
	else:
		water_timer_critic.wait_time = WATER_CRITIC_TIME
	## COMPOST ##
	var c_diff = dict["compost_normal"] - elapsed_time
	if c_diff > 0:
		compost_timer_normal.wait_time = c_diff
		compost_timer_normal.start()
	else:
		compost_timer_normal.stop()
	# 0.1 to give it time to send the has_died signal
	if compost_timer_normal.is_stopped():
		compost_timer_critic.wait_time = max(dict["compost_critic"] - (elapsed_time - dict["compost_normal"]), 0.1)
		compost_timer_critic.start()
	else:
		compost_timer_critic.wait_time = COMPOST_CRITIC_TIME


func _set_nickname(new_nick):
	nickname = new_nick
	emit_signal("nickname_changed")


func _load_model_by_phase():
	# delete previous model
	for i in get_children():
		if i.name == "Plant":
			i.queue_free()
	var model_scene = load("res://assets/models/plants/" + species.to_lower() + \
						"/phase_" + str(phase) + "/Phase" + str(phase) + ".tscn")
	var model
	if model_scene != null:
		model = model_scene.instance()
	else:
		# placeholder for debugging
		model = MeshInstance.new()
		model.mesh = PrismMesh.new()
	model.set_name("Plant")
	add_child(model)


func change_pot(pot_name):
	Globals.owned_pots[current_pot] += 1
	Globals.owned_pots[pot_name] -= 1
	var model_scene = load("res://assets/models/pots/" + pot_name.to_lower() + \
							"/" + pot_name.capitalize() + ".tscn")
	var model
	if model_scene != null:
		model = model_scene.instance()
	else:
		# placeholder for debugging
		model = MeshInstance.new()
		model.mesh = CubeMesh.new()
		var top = Spatial.new()
		top.set_name("Top")
		model.add_child(top)
	# delete current pot
	for i in get_children():
		if i.get_name().find("Pot") != -1:
			i.queue_free()
	current_pot = pot_name
	model.set_name("Pot")
	add_child(model)
	_place_plant_on_top()


func _place_plant_on_top():
	var plant = null
	var pot = null
	for i in get_children():
		if i.name.find("Plant") != -1:
			plant = i
		elif i.name.find("Pot") != -1:
			pot = i
	plant.transform = pot.get_node("Top").transform


func _update_production_value():
	production = PROD_BY_LEVEL[level]


func _update_phase_level():
	match phase:
		1:
			if level >= 3:
				phase = 2
				_load_model_by_phase()
				_place_plant_on_top()
		2:
			if level >= 7:
				phase = 3
				_load_model_by_phase()
				_place_plant_on_top()
		3:
			pass


func _apply_death_shader():
	var plant = get_node("Plant")
	plant.get_child(0).get_surface_material(0).albedo_color = Color(0.57, 0.40, 0, 1)


func _unlock_new_item():
	var menu = load("res://scenes/ui/ItemUnlockedMenu.tscn").instance()
	get_node("/root/Game/GameUI").add_child(menu)


func _setup_timers():
	# production timer
	prod_timer.wait_time = PRODUCTION_TIME
	prod_timer.start()
	# water timers
	water_timer_normal.wait_time = WATER_NORMAL_TIME
	water_timer_normal.start()
	water_timer_critic.wait_time = WATER_CRITIC_TIME
	# compost timers
	compost_timer_normal.wait_time = COMPOST_NORMAL_TIME
	compost_timer_normal.start()
	compost_timer_critic.wait_time = COMPOST_CRITIC_TIME


func _reset_timer(timer, must_start = true):
	var selected_time
	match timer.get_parent().get_name():
		"WaterTimers":
			if timer.get_name() == "Normal":
				selected_time = WATER_NORMAL_TIME
			else:
				selected_time = WATER_CRITIC_TIME
		"CompostTimers":
			if timer.get_name() == "Normal":
				selected_time = COMPOST_NORMAL_TIME
			else:
				selected_time = COMPOST_CRITIC_TIME
	timer.wait_time = selected_time
	if must_start:
		timer.start()
	else:
		timer.stop()


func _on_ProductionTimer_timeout():
	prod_timer.start()
	if alive:
		Globals.sollars += production


func _on_WaterNormal_timeout():
	# start critic counter
	water_timer_critic.start()


func _on_CompostNormal_timeout():
	# start critic counter
	compost_timer_critic.start()


func _on_WaterCritic_timeout():
	emit_signal("has_died")
	_apply_death_shader()
	alive = false


func _on_CompostCritic_timeout():
	emit_signal("has_died")
	_apply_death_shader()
	alive = false