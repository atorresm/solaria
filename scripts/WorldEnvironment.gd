extends WorldEnvironment


var time_speed = 1
const _max_time = 86400 # seconds in a day
const stages = ["DAY", "SUNRISE", "SUNSET", "NIGHT"]
const ambient_colors = {
	"DAY":Color(1, 1, 1, 1),
	"SUNRISE":Color(1, 0.95, 0.32, 1),
	"SUNSET":Color(0.87, 0.62, 0.04, 1),
	"NIGHT":Color(0.07, 0.19, 0.37)
	}
const back_colors = {
	"DAY":Color(0.53, 0.9, 0.9, 1),#Color(0.46, 0.75, 0.49, 1), 
	"SUNRISE":Color(0.88, 0.59, 0.43, 1),
	"SUNSET":Color(0.74, 0.59, 0.99, 1),
	"NIGHT":Color(0.12, 0.14, 0.2, 1)
}
var current_stage = null
# signals
signal night_started
signal night_finished


func _ready():
	var _current_time = 0.0
	var date_time = OS.get_datetime()
	_current_time += 3600 * date_time["hour"]
	_current_time += 60 * date_time["minute"]
	_current_time += date_time["second"]
	_choose_initial_stage()
	$DirectionalLight.light_color = ambient_colors[current_stage]
	var rotation = 360 * (_current_time / _max_time) - 90
	$DirectionalLight.rotation_degrees.x = -rotation


func _physics_process(delta):
	var rot = (360.0 * (1.0 / _max_time)) * delta * time_speed
	$DirectionalLight.rotation_degrees.x -= rot
	_choose_current_stage()


func _choose_initial_stage():
	match OS.get_datetime()["hour"]:
		3, 4, 5, 6, 7, 8:
			current_stage = "SUNRISE"
		9, 10, 11, 12, 13, 14:
			current_stage = "DAY"
		15, 16, 17, 18, 19, 20:
			current_stage = "SUNSET"
		21, 22, 23, 0, 1, 2:
			current_stage = "NIGHT"
			emit_signal("night_started")
	self.environment.background_color = back_colors[current_stage]


func _choose_current_stage():
	var prev_stage = current_stage
	var deg = (int($DirectionalLight.rotation_degrees.x) % 360)
	if (deg <= -315 and deg < 0) or (deg > -45 and deg < 0):
		# SUNRISE
		current_stage = "SUNRISE"
		if prev_stage != current_stage and prev_stage != null:
			_transition("NIGHT", "SUNRISE")
	elif deg <= -45 and deg > -135:
		# DAY
		current_stage = "DAY"
		if prev_stage != current_stage and prev_stage != null:
			_transition("SUNRISE", "DAY")
	elif deg <= -135 and deg > -225:
		# SUNSET
		current_stage = "SUNSET"
		if prev_stage != current_stage and prev_stage != null:
			_transition("DAY", "SUNSET")
	elif deg <= -225 and deg > -315:
		# NIGHT
		current_stage = "NIGHT"
		if prev_stage != current_stage and prev_stage != null:
			_transition("SUNSET", "NIGHT")


func _transition(from, to):
	# animate ambient color
	$Tween.interpolate_property($DirectionalLight, "light_color", ambient_colors[from], 
							ambient_colors[to], 10.0 / time_speed, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	# animate background color
	$Tween.interpolate_property(self.environment, "background_color", back_colors[from], 
							back_colors[to], 10.0 / time_speed, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	if to == "NIGHT":
		emit_signal("night_started")
	elif from == "NIGHT":
		emit_signal("night_finished")