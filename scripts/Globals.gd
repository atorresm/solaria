extends Node
# Software version
const VERSION = "1.0"


# Content enums
###############
const species = ["FICUS", "CHERRY_TREE", "BONSAI_MOYOGI", "CACTUS",
				"BONSAI_SHAKAN", "ORCHID", "SUNFLOWER", "ROSEMARY", "TULIP",
				"DAISY", "MYSTERIOUS"]
const pots = ["DEFAULTPOT", "CUP", "SIMPLE", "STICKMAN", "FLAT", "CACTUSPOT", "DIAMOND",
				"HANDPOT", "RADIOACTIVE", "SEA", "TOWER", "TROPHY", "VASE"]
const tracks = {"Solaria":"towntheme", "Etirwer":"etirwer", "Medieval Town":"medieval_town", 
				"Snowland Town":"snowland_town", "Calm":"calm", "Tropical":"tropical", "Starun":"starun"}
const statues = ["ARCHID", "SWAN", "FLYINGKICK", "HAND", "HUNTER",
				"LARGE", "SWORDMAN", "TREE", "ZOMBI"]
const phrases = ["PHRASE_1", "PHRASE_2", "PHRASE_3", "PHRASE_4", "PHRASE_5",
				"PHRASE_6", "PHRASE_7", "PHRASE_8", "PHRASE_9", "PHRASE_10",
				"PHRASE_11", "PHRASE_12", "PHRASE_13", "PHRASE_14", "PHRASE_15",
				"PHRASE_16", "PHRASE_17", "PHRASE_18", "PHRASE_19", "PHRASE_20",
				"PHRASE_21", "PHRASE_22", "PHRASE_23", "PHRASE_24", "PHRASE_25",
				"PHRASE_26", "PHRASE_27", "PHRASE_28", "PHRASE_29", "PHRASE_30"]

# Prices
######## 
const SEED_PRICES = {"FICUS":0, "CHERRY_TREE":1000, "BONSAI_MOYOGI":1500, "CACTUS":750,
					"ORCHID":750, "SUNFLOWER":300, "ROSEMARY":300,
					"TULIP":750, "DAISY":300, "BONSAI_SHAKAN":1500, "MYSTERIOUS":3000}
const TRACKS_PRICES = {"Solaria":100, "Etirwer":100, "Medieval Town":100, "Snowland Town":200,
					"Calm":150, "Tropical":100, "Starun":300}
const POTS_PRICES = {"DEFAULTPOT":0, "CUP":300, "SIMPLE":100, "STICKMAN":1000,
					"FLAT":300, "CACTUSPOT":500, "DIAMOND":2000, "HANDPOT":700,
					"RADIOACTIVE":600, "SEA":300, "TOWER":250, "TROPHY":3000, "VASE":500}
const STATUES_PRICES = {"ARCHID":500, "SWAN":100, "FLYINGKICK":700, "HAND":400,
						"ZOMBI":200, "HUNTER":500, "LARGE":450, "SWORDMAN":1000, "TREE":500}

# Owned/unlocked items
######################
var PLANT_ID_SEQ
# pots
# pot:amount
var owned_pots
var pots_to_buy
# seeds
# owned seeds are stored in the Seeds nodes (those with amount > 0)
var seeds_to_buy
# tracks
var owned_tracks
var tracks_to_buy
# statues
var owned_statues
var statues_to_buy
# number of sollars
var sollars setget set_sollars
# current track sounding
var current_track
# current statue used
var current_statue

func _ready():
	randomize()
	_load_globals()
	_load_seeds()
	_load_composts()
	_load_plants()
	_load_timers()
	play_music(current_track)
	#_debug_init()


func _notification(what):
	match what:
		MainLoop.NOTIFICATION_WM_QUIT_REQUEST, MainLoop.NOTIFICATION_WM_FOCUS_OUT, MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST, \
	 	MainLoop.NOTIFICATION_WM_UNFOCUS_REQUEST:
			_save_globals()
			_save_timers()
			_save_plants()
			_save_seeds()
			_save_composts()
		MainLoop.NOTIFICATION_WM_FOCUS_IN:
			_load_timers()


func set_sollars(val):
	sollars = min(99999, val)


func get_new_plant_id():
	PLANT_ID_SEQ += 1
	return PLANT_ID_SEQ - 1


func play_sound(id):
	$SoundEffectPlayer.stream = load("res://assets/sounds/effects/" + id  + ".ogg")
	$SoundEffectPlayer.play()


func play_music(id):
	if $MusicPlayer.playing:
		$MusicPlayer.stop()
	$MusicPlayer.stream = load("res://assets/sounds/music/" + tracks[id] + ".ogg")
	current_track = id
	$MusicPlayer.play()


func fade_in(obj, hiding = false):
	$Tween.interpolate_property(obj, "modulate", Color(1, 1, 1, 0), 
								Color(1, 1, 1, 1), 0.15, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	if hiding:
		obj.show()
	$Tween.start()


func fade_out(obj, hiding = false):
	$Tween.interpolate_property(obj, "modulate", Color(1, 1, 1, 1), 
								Color(1, 1, 1, 0), 0.15, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	if hiding:
		obj.hide()
	else:
		obj.queue_free()


func _load_globals():
	var savefile = File.new()
	if savefile.file_exists("user://globals.sav"):
		# open the file and load it
		savefile.open("user://globals.sav", File.READ)
		while not savefile.eof_reached():
			var current_line = parse_json(savefile.get_line())
			if current_line != null:
				for i in current_line.keys():
					set(i, current_line[i])
	else:
		# load defaults
		sollars = 1000
		PLANT_ID_SEQ = 0
		owned_pots = {}
		for i in pots:
			if i == "DEFAULTPOT":
				owned_pots[i] = 1
			else:
				owned_pots[i] = 0 
		pots_to_buy = ["DEFAULTPOT"]
		# seeds set in _load_seeds
		owned_tracks = ["Solaria"]
		tracks_to_buy = []
		owned_statues = []
		statues_to_buy = []
		current_track = "Solaria"
		current_statue = null


func _save_globals():
	var file = File.new()
	file.open("user://globals.sav", File.WRITE)
	var save_data = {
		"sollars":sollars,
		"PLANT_ID_SEQ":PLANT_ID_SEQ,
		"owned_pots":owned_pots,
		"pots_to_buy":pots_to_buy,
		# owned seeds seeds_to_buy are stored in seeds.sav
		"owned_tracks":owned_tracks,
		"tracks_to_buy":tracks_to_buy,
		"owned_statues":owned_statues,
		"statues_to_buy":statues_to_buy,
		"current_track":current_track,
		"current_statue":current_statue
	}
	file.store_line(to_json(save_data))
	file.close()


func _load_seeds():
	var savefile = File.new()
	if savefile.file_exists("user://seeds.sav"):
		# savefile exists, load with it
		savefile.open("user://seeds.sav", File.READ)
		while not savefile.eof_reached():
			var current_line = parse_json(savefile.get_line())
			if current_line != null:
				if current_line.has("seeds_to_buy"):
					seeds_to_buy = current_line["seeds_to_buy"]
					continue
				var plant_seed = load("res://scenes/Seed.tscn").instance()
				plant_seed.init(current_line["species"])
				plant_seed.amount = current_line["amount"]
				plant_seed.price = SEED_PRICES[current_line["species"]]
				$Seeds.add_child(plant_seed)
		savefile.close()
	else:
		# load defaults
		for i in species:
			var plant_seed = load("res://scenes/Seed.tscn").instance()
			plant_seed.init(i)
			if i == "FICUS":
				plant_seed.amount = 1
			else:
				plant_seed.amount = 0
			plant_seed.price = SEED_PRICES[i]
			$Seeds.add_child(plant_seed)
		seeds_to_buy = ["FICUS"]


func _save_seeds():
	var file = File.new()
	file.open("user://seeds.sav", File.WRITE)
	for i in $Seeds.get_children():
		var seed_data = {
			"species":i.species,
			"amount":i.amount
		}
		file.store_line(to_json(seed_data))
	# add seeds_to_buy array
	file.store_line(to_json({"seeds_to_buy":seeds_to_buy}))
	file.close()


func _load_composts():
	var savefile = File.new()
	var small_compost = load("res://scenes/Compost.tscn").duplicate(true).instance()
	small_compost.name = "Small"
	var medium_compost = load("res://scenes/Compost.tscn").duplicate(true).instance()
	medium_compost.name = "Medium"
	var big_compost = load("res://scenes/Compost.tscn").duplicate(true).instance()
	big_compost.name = "Big"
	if savefile.file_exists("user://composts.sav"):
		# load save
		savefile.open("user://composts.sav", File.READ)
		while not savefile.eof_reached():
			var current_line = parse_json(savefile.get_line())
			if current_line != null:
				match current_line["type"]:
					"Small":
						small_compost.percentage = current_line["percentage"]
						small_compost.amount = current_line["amount"]
						small_compost.price = current_line["price"]
					"Medium":
						medium_compost.percentage = current_line["percentage"]
						medium_compost.amount = current_line["amount"]
						medium_compost.price = current_line["price"]
					"Big":
						big_compost.percentage = current_line["percentage"]
						big_compost.amount = current_line["amount"]
						big_compost.price = current_line["price"]
		savefile.close()
	else:
		# default options
		small_compost.percentage = 0.3
		small_compost.amount = 5
		small_compost.price = 100
		medium_compost.percentage = 0.6
		medium_compost.amount = 0
		medium_compost.price = 150
		big_compost.percentage = 0.9
		big_compost.amount = 0
		big_compost.price = 200
	$Composts.add_child(small_compost)
	$Composts.add_child(medium_compost)
	$Composts.add_child(big_compost)


func _save_composts():
	var file = File.new()
	file.open("user://composts.sav", File.WRITE)
	for i in $Composts.get_children():
		var save_data = {
			"type":i.name,
			"percentage":i.percentage,
			"amount":i.amount,
			"price":i.price
		}
		file.store_line(to_json(save_data))


func _load_plants():
	var plants_file = File.new()
	if not plants_file.file_exists("user://plants.sav"):
		return null
	plants_file.open("user://plants.sav", File.READ)
	while not plants_file.eof_reached():
		var current_line = parse_json(plants_file.get_line())
		if current_line != null:
			var new_plant = load("res://scenes/Plant.tscn").instance()
			$InactivePlants.add_child(new_plant)
			new_plant.load_plant(current_line)
	plants_file.close()


func _save_plants():
	var plants_file = File.new()
	plants_file.open("user://plants.sav", File.WRITE)
	for plant in get_tree().get_nodes_in_group("Plants"):
		var plant_data = plant.save_plant()
		plants_file.store_line(to_json(plant_data))
	plants_file.close()


func _load_timers():
	var timers_file = File.new()
	if not timers_file.file_exists("user://timers.sav"):
		return null
	timers_file.open("user://timers.sav", File.READ)
	while not timers_file.eof_reached():
		var current_line = parse_json(timers_file.get_line())
		if current_line != null:
			for i in get_tree().get_nodes_in_group("Plants"):
				if i.plant_id == current_line["plant_id"]:
					i.load_timers(current_line)
	timers_file.close()


func _save_timers():
	var timers_file = File.new()
	timers_file.open("user://timers.sav", File.WRITE)
	for plant in get_tree().get_nodes_in_group("Plants"):
		var timer_data = plant.save_timers()
		timers_file.store_line(to_json(timer_data))
	timers_file.close()


func _debug_init():
	sollars = 99999
	tracks_to_buy.append("Solaria")
	tracks_to_buy.append("Etirwer")
	tracks_to_buy.append("Medieval Town")
	tracks_to_buy.append("Snowland Town")
	seeds_to_buy.append("CACTUS")
	seeds_to_buy.append("SUNFLOWER")
	seeds_to_buy.append("ROSEMARY")
	seeds_to_buy.append("BONSAI_MOYOGI")
	seeds_to_buy.append("ACANTHUS")
	seeds_to_buy.append("DAISY")
	seeds_to_buy.append("ORCHID")
	seeds_to_buy.append("TULIP")
	seeds_to_buy.append("CHERRY_TREE")
	statues_to_buy.append("ARCHID")
	statues_to_buy.append("SWAN")
	statues_to_buy.append("FLYINGKICK")
	pots_to_buy.append("CUP")
	pots_to_buy.append("SIMPLE")
	pots_to_buy.append("STICKMAN")