extends Control

const pages = {0:"COMPOSTS_LABEL", 1:"SEEDS_LABEL", 2:"POTS_LABEL", 3:"STATUES_LABEL", 4:"MUSIC_LABEL"}
var current_page = 0

func _ready():
	reload_current_page()
	Globals.fade_in(self)


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if not get_global_rect().has_point(event.position):
			Globals.play_sound("close")
			Globals.fade_out(self)


func reload_current_page():
	# remove previous menu
	for i in $Container.get_children():
		if i.name.find("CurrentMenu") != -1:
			i.queue_free()
			break
	var menu
	match current_page:
		0:
			menu = load("res://scenes/ui/ShopCompostMenu.tscn").instance()
		1:
			menu = load("res://scenes/ui/SeedListMenu.tscn").instance()
			menu.init(null)
		2:
			menu = load("res://scenes/ui/PotListMenu.tscn").instance()
			menu.init(null)
		3:
			menu = load("res://scenes/ui/StatuesListMenu.tscn").instance()
			menu.init(true)
		4:
			menu = load("res://scenes/ui/TrackListMenu.tscn").instance()
			menu.init(true)
	menu.set_name("CurrentMenu")
	$Container/Navigation/Current.text = tr(pages[current_page])
	$Container.add_child(menu)


func _on_Prev_pressed():
	Globals.play_sound("click")
	if current_page == 0:
		current_page = pages.size() - 1
	else:
		current_page -= 1
	reload_current_page()


func _on_Next_pressed():
	Globals.play_sound("click")
	if current_page == pages.size() - 1:
		current_page = 0
	else:
		current_page += 1
	reload_current_page()