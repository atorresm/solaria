extends Control

const ENTRIES_PER_PAGE = 8
var entries = []
var current_page = 1
var total_pages = 1
var shop_mode


func _ready():
	if shop_mode:
		# will spawn as a child
		get_rect().position = Vector2(0, 0)
		for i in $Container/Buttons.get_children():
			i.size_flags_horizontal = SIZE_SHRINK_CENTER
		$UIPanel.hide()
	_fill_info()
	_show_page(1)
	if not shop_mode:
		Globals.fade_in(self)


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed() and not shop_mode:
		if $Container/NoTracks.is_visible_in_tree() or not get_global_rect().has_point(event.position):
			Globals.play_sound("close")
			Globals.fade_out(self)


func init(shop_mode):
	self.shop_mode = shop_mode


func _fill_info():
	entries = []
	var page_counter = 1
	var counter = 0
	var iter
	if shop_mode:
		iter = Globals.tracks_to_buy
	else:
		iter = Globals.owned_tracks
	for i in iter:
		if i == "Solaria" and shop_mode:
			continue # default song, don't list on shop
		if counter >= ENTRIES_PER_PAGE:
			counter = 0
			page_counter += 1
		var entry = load("res://scenes/ui/TrackListEntry.tscn").duplicate(true).instance()
		entry.init(i, page_counter, shop_mode)
		entry.set_name("Entry" + str(counter) + "_" + str(page_counter))
		entries.append(entry)
		counter += 1
	total_pages = page_counter


func _show_page(index):
	# remove current entries (if any)
	for i in $Container.get_children():
		if i.get_name().begins_with("Entry"):
			$Container.remove_child(i)
	# setup buttons
	match index:
		1:
			$Container/Buttons/Prev.disabled = true
			$Container/Buttons/Next.disabled = false
		total_pages:
			$Container/Buttons/Prev.disabled = false
			$Container/Buttons/Next.disabled = true
		_:
			$Container/Buttons/Prev.disabled = false
			$Container/Buttons/Next.disabled = false
	# setup label
	$Container/Buttons/Label.text = str(index) + " / " + str(total_pages)
	# hide buttons and label if there is only one page
	if total_pages == 1:
		$Container/Buttons.hide()
	# add page entries
	for i in entries:
		if i.list_page == index:
			$Container.add_child(i)
	if entries.size() == 0:
		var extra_label
		if shop_mode:
			extra_label = tr("HOW_TO_UNLOCK_LABEL")
		else:
			extra_label = tr("CHECK_SHOP_LABEL")
		$Container/Buttons.hide()
		$Container/NoTracks.text = tr("NO_TRACKS_LABEL") + "\n" + extra_label
		$Container/NoTracks.show()


func _on_Prev_pressed():
	Globals.play_sound("click")
	_show_page(current_page - 1)
	current_page -= 1


func _on_Next_pressed():
	Globals.play_sound("click")
	_show_page(current_page + 1)
	current_page += 1