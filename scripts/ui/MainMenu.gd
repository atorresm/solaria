extends Control


func _ready():
	$Version.text = Globals.VERSION


func _physics_process(delta):
	pass


func _on_Start_pressed():
	Globals.fade_out(self)


func _on_Credits_pressed():
	var menu = load("res://scenes/ui/CreditsMenu.tscn").instance()
	menu.name = "CreditsMenu"
	add_child(menu)

func _on_Help_pressed():
	var menu = load("res://scenes/ui/HelpMenu.tscn").instance()
	menu.name = "HelpMenu"
	add_child(menu)
