extends Control

var start_pos = Vector2()

func _ready():
		# anim visibility
	$Tween.interpolate_property(self, "modulate", Color(1, 1, 1, 1), 
							Color(1, 1, 1, 0), 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	# anim pos
	var end_pos = Vector2(start_pos.x, start_pos.y - 50)
	$Tween.interpolate_property(self, "rect_position", start_pos, end_pos,
							1.0, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	queue_free()


func init(start_pos):
	self.start_pos = start_pos