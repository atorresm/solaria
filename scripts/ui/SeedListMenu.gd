extends Control

const SEEDS_PER_PAGE = 8
var spot
var entries = []
var current_page = 1
var total_pages = 1
var shop_mode = false


func _ready():
	if shop_mode:
		# will spawn as a child
		get_rect().position = Vector2(0, 0)
		for i in $Container/Buttons.get_children():
			i.size_flags_horizontal = SIZE_SHRINK_CENTER
		$UIPanel.hide()
	_fill_info()
	_show_page(1)
	if not shop_mode:
		Globals.fade_in(self)


func init(spot):
	self.spot = spot
	self.shop_mode = spot == null


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed() and not shop_mode:
		if $Container/NoSeeds.is_visible_in_tree() or not get_global_rect().has_point(event.position):
			Globals.play_sound("close")
			Globals.fade_out(self)


func _fill_info():
	var seeds = []
	# select seeds to show
	for i in Globals.get_node("Seeds").get_children():
		if shop_mode:
			# don't show default seed in shop
			if i.species == "FICUS":
				continue
			# only show seeds unlocked to buy
			if i.species in Globals.seeds_to_buy:
				seeds.append(i)
		else:
			# only show seeds with amount > 0
			if i.amount > 0:
				seeds.append(i)
	var page_counter = 1
	var seed_counter = 0
	for i in seeds:
		if seed_counter >= SEEDS_PER_PAGE:
			seed_counter = 0
			page_counter += 1
		var entry = load("res://scenes/ui/SeedListEntry.tscn").duplicate(true).instance()
		entry.init(i, spot, page_counter)
		entry.set_name("Entry" + str(seed_counter) + "_" + str(page_counter))
		entries.append(entry)
		seed_counter += 1
	total_pages = page_counter


func _show_page(index):
	# remove current entries (if any)
	for i in $Container.get_children():
		if i.get_name().begins_with("Entry"):
			$Container.remove_child(i)
	# setup buttons
	match index:
		1:
			$Container/Buttons/Prev.disabled = true
			$Container/Buttons/Next.disabled = false
		total_pages:
			$Container/Buttons/Prev.disabled = false
			$Container/Buttons/Next.disabled = true
		_:
			$Container/Buttons/Prev.disabled = false
			$Container/Buttons/Next.disabled = false
	# setup label
	$Container/Buttons/Label.text = str(index) + " / " + str(total_pages)
	# hide buttons and label if there is only one page
	if total_pages == 1:
		$Container/Buttons.hide()
	# add page entries
	for i in entries:
		if i.list_page == index:
			$Container.add_child(i)
	if entries.size() == 0:
		var extra_label
		if shop_mode:
			extra_label = tr("HOW_TO_UNLOCK_LABEL")
		else:
			extra_label = tr("CHECK_SHOP_LABEL")
		$Container/Buttons.hide()
		$Container/NoSeeds.text = tr("NO_SEEDS_LABEL") + "\n" + extra_label
		$Container/NoSeeds.show()


func _on_Prev_pressed():
	Globals.play_sound("click")
	_show_page(current_page - 1)
	current_page -= 1


func _on_Next_pressed():
	Globals.play_sound("click")
	_show_page(current_page + 1)
	current_page += 1