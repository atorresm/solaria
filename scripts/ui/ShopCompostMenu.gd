extends VBoxContainer

var small = Globals.get_node("Composts/Small")
var medium = Globals.get_node("Composts/Medium")
var big = Globals.get_node("Composts/Big")

func _ready():
	# setup labels
	$Prices/Small/Price.text = str(small.price)
	$Prices/Medium/Price.text = str(medium.price)
	$Prices/Big/Price.text = str(big.price)
	$Amounts/Small.text = "x" + str(small.amount)
	$Amounts/Medium.text = "x" + str(medium.amount)
	$Amounts/Big.text = "x" + str(big.amount)
	_setup_menu()


func _physics_process(delta):
	_setup_menu()


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		for i in $Buttons.get_children():
			if i.get_global_rect().has_point(event.position): 
				if i.disabled:
					Globals.play_sound("error")
				else:
					get_node("/root/Game/GameUI").show_confirm_anim(event.position)


func _setup_menu():
	# enable/disable buttons
	$Buttons/Small.disabled = small.price > Globals.sollars
	$Buttons/Medium.disabled = medium.price > Globals.sollars
	$Buttons/Big.disabled = big.price > Globals.sollars
	# update labels
	$Amounts/Small.text = "x" + str(small.amount)
	$Amounts/Medium.text = "x" + str(medium.amount)
	$Amounts/Big.text = "x" + str(big.amount)


func _on_Small_pressed():
	Globals.play_sound("purchase")
	small.amount += 1
	Globals.sollars -= small.price
	_setup_menu()


func _on_Medium_pressed():
	Globals.play_sound("purchase")
	medium.amount += 1
	Globals.sollars -= medium.price
	_setup_menu()


func _on_Big_pressed():
	Globals.play_sound("purchase")
	big.amount += 1
	Globals.sollars -= big.price
	_setup_menu()