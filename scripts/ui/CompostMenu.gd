extends Control


var plant = null
var small_compost = Globals.get_node("Composts/Small")
var medium_compost = Globals.get_node("Composts/Medium")
var big_compost = Globals.get_node("Composts/Big")

func _ready():
	_populate_compost_menu()
	Globals.fade_in(self)


func init(plant):
	self.plant = plant


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if not get_rect().has_point(event.position):
			Globals.play_sound("close")
			Globals.fade_out(self)


func _populate_compost_menu():
	$Container/Amounts/Small.text = tr("SMALL_COMPOST") + "\n" +  "x" + str(small_compost.amount)
	$Container/Amounts/Medium.text = tr("MEDIUM_COMPOST") + "\n" + "x" + str(medium_compost.amount)
	$Container/Amounts/Big.text = tr("BIG_COMPOST") + "\n" + "x" + str(big_compost.amount)
	if small_compost.amount == 0:
		$Container/Buttons/Small.disabled = true
	if medium_compost.amount == 0:
		$Container/Buttons/Medium.disabled = true
	if big_compost.amount == 0:
		$Container/Buttons/Big.disabled = true


func _on_Small_pressed():
	Globals.play_sound("click")
	plant.apply_compost(small_compost)
	Globals.fade_out(self)


func _on_Medium_pressed():
	Globals.play_sound("click")
	plant.apply_compost(medium_compost)
	Globals.fade_out(self)


func _on_Big_pressed():
	Globals.play_sound("click")
	plant.apply_compost(big_compost)
	Globals.fade_out(self)