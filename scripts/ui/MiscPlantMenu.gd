extends Control

var plant = null
var spot = null
onready var UI = get_node("/root/Game/GameUI")


func _ready():
	Globals.fade_in(self)


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if not get_rect().has_point(event.position):
			Globals.play_sound("close")
			Globals.fade_out(self)


func init(plant, spot):
	self.plant = plant
	self.spot = spot


func _remove_PlantMenu():
	for i in UI.get_children():
		if i.get_name().find("PlantMenu") != -1:
			Globals.fade_out(i)


func _on_Change_pressed():
	_remove_PlantMenu()
	var list_inactive = load("res://scenes/ui/PlantListMenu.tscn").instance()
	list_inactive.init(spot, plant)
	UI.add_child(list_inactive)
	Globals.play_sound("click")
	Globals.fade_out(self)


func _on_Remove_pressed():
	_remove_PlantMenu()
	# move plant to inactive
	spot.remove_child(plant)
	Globals.get_node("InactivePlants").add_child(plant)
	Globals.play_sound("click")
	Globals.fade_out(self)


func _on_Pot_pressed():
	_remove_PlantMenu()
	var menu = load("res://scenes/ui/PotListMenu.tscn").instance()
	menu.init(plant)
	UI.add_child(menu)
	Globals.play_sound("click")
	Globals.fade_out(self)