extends Control

onready var UI = get_node("/root/Game/GameUI")

func _ready():
	Globals.fade_in(self)


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if not get_rect().has_point(event.position):
			Globals.play_sound("close")
			Globals.fade_out(self)


func _on_Shop_pressed():
	var menu = load("res://scenes/ui/ShopMenu.tscn").instance()
	UI.add_child(menu)
	Globals.play_sound("click")
	Globals.fade_out(self)


func _on_Tracks_pressed():
	var menu = load("res://scenes/ui/TrackListMenu.tscn").instance()
	menu.init(false)
	UI.add_child(menu)
	Globals.play_sound("click")
	Globals.fade_out(self)


func _on_Help_pressed():
	var menu = load("res://scenes/ui/HelpMenu.tscn").instance()
	menu.name = "HelpMenu"
	UI.add_child(menu)
	Globals.play_sound("click")
	Globals.fade_out(self)
