extends Control

func _ready():
	$Main.text = "Solaria, 2018\n" + \
				tr("DEVELOPED_FOR_LABEL") + " CUSL XII\n" + \
				tr("MODELING_LABEL") + ":\n" + \
				"Jaime Torres Moríñigo\n" + \
				tr("PROGRAMMING_LABEL") + ":\n" + \
				"Antonio Torres Moríñigo\n" + \
				tr("LICENSED_LABEL") + "\n" + \
				tr("GODOT_LABEL")
	Globals.fade_in(self)


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if not get_global_rect().has_point(event.position):
			Globals.play_sound("close")
			Globals.fade_out(self)


func _on_Accept_pressed():
	Globals.play_sound("close")
	Globals.fade_out(self)
