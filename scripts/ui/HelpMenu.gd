extends Control

func _ready():
	Globals.fade_in(self)


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if not get_global_rect().has_point(event.position):
			Globals.play_sound("close")
			Globals.fade_out(self)


func _on_Accept_pressed():
	Globals.play_sound("close")
	Globals.fade_out(self)
