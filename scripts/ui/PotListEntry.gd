extends Control


var list_page
var pot_name
var shop_mode
var plant


func _ready():
	$Container/Labels/Name.text = pot_name
	if shop_mode:
		$Container/Labels/Price/Label.text = str(Globals.POTS_PRICES[pot_name])
	else:
		if not pot_name == "DEFAULTPOT":
			$Container/Labels/Price/Label.text = "x" + str(Globals.owned_pots[pot_name])
		else:
			$Container/Labels/Price.hide()
		$Container/Labels/Price/TextureRect.hide()
	$Container/Icon.texture = load("res://assets/ui/" + pot_name.to_lower() + "_icon.png")


func _physics_process(delta):
	if shop_mode:
		if Globals.sollars <= Globals.POTS_PRICES[pot_name]:
			$BrownButtonPanel.make_inactive()
		else:
			$BrownButtonPanel.make_active()


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if get_global_rect().has_point(event.position):
			if shop_mode:
				if Globals.sollars >= Globals.POTS_PRICES[pot_name]:
					Globals.play_sound("purchase")
					Globals.owned_pots[pot_name] = Globals.owned_pots[pot_name] + 1
					Globals.sollars -= Globals.POTS_PRICES[pot_name]
					get_node("/root/Game/GameUI/ShopMenu").reload_current_page()
					get_node("/root/Game/GameUI").show_confirm_anim(event.position)
				else:
					Globals.play_sound("error")
			else:
				Globals.play_sound("click")
				plant.change_pot(pot_name)
				Globals.fade_out(get_parent().get_parent())


func init(pot_name, list_page, plant):
	self.pot_name = pot_name
	self.list_page = list_page
	self.plant = plant
	self.shop_mode = plant == null