extends Control

var spot = null


func _ready():
	Globals.fade_in(self)


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if not get_global_rect().has_point(event.position):
			Globals.play_sound("close")
			Globals.fade_out(self)


func init(spot):
	self.spot = spot


func _on_Seed_pressed():
	var menu = load("res://scenes/ui/SeedListMenu.tscn").instance()
	menu.init(spot)
	# parent is UI node
	get_parent().add_child(menu)
	Globals.play_sound("click")
	Globals.fade_out(self)


func _on_Plant_pressed():
	var menu = load("res://scenes/ui/PlantListMenu.tscn").instance()
	menu.init(spot)
	get_parent().add_child(menu)
	Globals.play_sound("click")
	Globals.fade_out(self)