extends Control

var plant_seed
var spot
var list_page
var shop_mode


func _ready():
	$Container/Icon.texture = load("res://assets/ui/plant_seed.png")
	if not shop_mode:
		$Container/Labels/AmountPrice/TextureRect.hide()


func _physics_process(delta):
	if shop_mode:
		if Globals.sollars < Globals.SEED_PRICES[plant_seed.species]:
			$BrownButtonPanel.make_inactive()
		else:
			$BrownButtonPanel.make_active()


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
			if get_global_rect().has_point(event.position):
				if not shop_mode:
					Globals.play_sound("click")
					var plant = load("res://scenes/Plant.tscn").duplicate(true).instance()
					plant.init(plant_seed.species)
					# reduce seed amount (if not default)
					if not plant_seed.species == "FICUS":
						plant_seed.amount -= 1
					spot.add_child(plant)
					# quit the plant selection screen
					Globals.fade_out(get_parent().get_parent())
				else:
					if Globals.sollars >= plant_seed.price:
						Globals.play_sound("purchase")
						plant_seed.amount += 1
						Globals.sollars -= plant_seed.price
						get_node("/root/Game/GameUI").show_confirm_anim(event.position)
					else:
						Globals.play_sound("error")


func init(plant_seed, spot, list_page):
	self.plant_seed = plant_seed
	self.spot = spot
	self.list_page = list_page
	self.shop_mode = spot == null
	# TODO set icon
	$Container/Labels/Species.text = tr(plant_seed.species)
	if shop_mode:
		$Container/Labels/AmountPrice/Label.text = str(plant_seed.price)
	else:
		if plant_seed.species != "FICUS":
			$Container/Labels/AmountPrice/Label.text = "x" + str(plant_seed.amount)
		else:
			$Container/Labels/AmountPrice.hide()