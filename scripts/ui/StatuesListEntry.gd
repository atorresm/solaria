extends Control


var list_page
var statue_name
var shop_mode


func _ready():
	$Container/Labels/Name.text = statue_name
	$Container/Labels/Price/Label.text = str(Globals.STATUES_PRICES[statue_name])
	if not shop_mode:
		$Container/Labels/Price.hide()
	$Container/Icon.texture = load("res://assets/ui/" + statue_name.to_lower() + "_icon.png")


func _physics_process(delta):
	if shop_mode:
		if Globals.sollars < Globals.STATUES_PRICES[statue_name]:
			$BrownButtonPanel.make_inactive()
		else:
			$BrownButtonPanel.make_active()


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if get_global_rect().has_point(event.position):
			if shop_mode:
				if Globals.sollars >= Globals.STATUES_PRICES[statue_name]:
					Globals.play_sound("purchase")
					var statue_index = Globals.statues_to_buy.find(statue_name)
					Globals.statues_to_buy.remove(statue_index)
					Globals.owned_statues.append(statue_name)
					Globals.sollars -= Globals.STATUES_PRICES[statue_name]
					get_node("/root/Game/GameUI/ShopMenu").reload_current_page()
					get_node("/root/Game/GameUI").show_confirm_anim(event.position)
				else:
					Globals.play_sound("error")
			else:
				Globals.play_sound("click")
				get_node("/root/Game/Garden").place_statue_on_fountain(statue_name)
				Globals.fade_out(get_parent().get_parent())


func init(statue_name, list_page, shop_mode):
	self.statue_name = statue_name
	self.list_page = list_page
	self.shop_mode = shop_mode