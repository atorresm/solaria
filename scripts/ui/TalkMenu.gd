extends Control

var plant = null


func _ready():
	$Container/Header.text = plant.nickname + " " + tr("SAYS_LABEL") + ":"
	$Container/Phrase.text = _get_random_phrase()
	Globals.fade_in(self)


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if not get_global_rect().has_point(event.position):
			Globals.play_sound("close")
			Globals.fade_out(self)


func init(plant):
	self.plant = plant


func _get_random_phrase():
	return tr(Globals.phrases[randi() % Globals.phrases.size()])