extends Control

var overlay_showing = false


func _ready():
	Globals.fade_in(self, true)


func _physics_process(delta):
	$Sollars/Container/Label.text = "%05d" % Globals.sollars


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed() and not has_node("MainMenu"):
		if get_child_count() == 3: # 3 childs only: More, Sollars and UITimer
			if overlay_showing:
				if not $More.get_global_rect().has_point(event.position):
					_hide_overlay()
			else:
				_show_overlay()
				$UITimer.start()


func show_plant_menu(plant, spot):
	var new_panel = load("res://scenes/ui/PlantMenu.tscn").instance()
	new_panel.set_name("PlantMenu")
	new_panel.init(plant, spot)
	add_child(new_panel)


func show_spot_creation(spot):
	var menu = load("res://scenes/ui/SpotCreationMenu.tscn").instance()
	menu.init(spot)
	add_child(menu)


func show_statue_selection_menu():
	var menu = load("res://scenes/ui/StatuesListMenu.tscn").instance()
	menu.init(false)
	add_child(menu)


func show_confirm_anim(start_pos):
	var anim = load("res://scenes/ui/ConfirmAnimation.tscn").instance()
	anim.init(start_pos)
	add_child(anim)


func _show_overlay():
	overlay_showing = true
	Globals.fade_in($More, true)
	Globals.fade_in($Sollars, true)


func _hide_overlay():
	overlay_showing = false
	Globals.fade_out($More, true)
	Globals.fade_out($Sollars, true)


func _on_UITimer_timeout():
	# hide all children
	for i in get_children():
		# todo fadeout anim
		if ["Sollars", "More"].has(i.get_name()):
			# don't hide Sollars if shopmenu is showing
			if i.name == "Sollars" and has_node("ShopMenu"):
				continue
			Globals.fade_out(i, true)
		elif i.get_name() == "UITimer":
			continue


func _on_sollar_amount_updated():
	$Sollars/Container/Label.text = str(Globals.sollars)


func _on_More_pressed():
	var menu = load("res://scenes/ui/MoreMenu.tscn").instance()
	$More.hide()
	Globals.play_sound("click")
	add_child(menu)
