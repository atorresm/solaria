extends Control

const PLANTS_PER_PAGE = 8
var spot
var plant_to_sustitute # plant to sustitute (if any)
var entries = []
var current_page = 1
var total_pages = 1


func _ready():
	_fill_info()
	_show_page(1)
	Globals.fade_in(self)


func init(spot, plant_to_sustitute = null):
	self.spot = spot
	self.plant_to_sustitute = plant_to_sustitute


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if $Container/NoPlants.is_visible_in_tree() or not get_rect().has_point(event.position):
			Globals.play_sound("close")
			Globals.fade_out(self)


func _fill_info():
	var plants = Globals.get_node("InactivePlants").get_children()
	var page_counter = 1
	var plant_counter = 0
	for i in plants:
		if plant_counter >= PLANTS_PER_PAGE:
			plant_counter = 0
			page_counter += 1
		var entry = load("res://scenes/ui/PlantListEntry.tscn").duplicate(true).instance()
		entry.init(i, spot, page_counter, plant_to_sustitute)
		entry.set_name("Entry" + str(plant_counter) + "_" + str(page_counter))
		entries.append(entry)
		plant_counter += 1
	total_pages = page_counter


func _show_page(index):
	# remove current entries (if any)
	for i in $Container.get_children():
		if i.get_name().begins_with("Entry"):
			$Container.remove_child(i)
	# setup buttons
	match index:
		1:
			$Container/Buttons/Prev.disabled = true
			$Container/Buttons/Next.disabled = false
		total_pages:
			$Container/Buttons/Prev.disabled = false
			$Container/Buttons/Next.disabled = true
		_:
			$Container/Buttons/Prev.disabled = false
			$Container/Buttons/Next.disabled = false
	# setup label
	$Container/Buttons/Label.text = str(index) + " / " + str(total_pages)
	# hide buttons and label if there is only one page
	if total_pages == 1:
		$Container/Buttons.hide()
	# add page entries
	for i in entries:
		if i.list_page == index:
			$Container.add_child(i)
	if entries.size() == 0:
		$Container/Buttons.hide()
		$Container/NoPlants.show()


func _on_Prev_pressed():
	Globals.play_sound("click")
	_show_page(current_page - 1)
	current_page -= 1


func _on_Next_pressed():
	Globals.play_sound("click")
	_show_page(current_page + 1)
	current_page += 1