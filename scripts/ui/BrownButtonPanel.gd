extends NinePatchRect

func _ready():
	pass

func make_inactive():
	self.texture = load("res://assets/ui/buttonSquare_blue.png")

func make_active():
	self.texture = load("res://assets/ui/buttonSquare_brown.png")
