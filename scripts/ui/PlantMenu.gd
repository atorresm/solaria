extends Control

var plant = null
var spot = null
const dependent_menus = ["CompostMenu", "NameEditMenu", "MiscPlantMenu", "TalkMenu"]
onready var UI = get_node("/root/Game/GameUI")


func _ready():
	# connect plant signals
	plant.connect("exp_points_changed", self, "_on_Plant_exp_points_changed")
	plant.connect("nickname_changed", self, "_on_Plant_nickname_changed")
	plant.connect("has_died", self, "_on_Plant_has_died")
	_fill_info()
	if not plant.alive:
		$Info.hide()
		$SecondColumn.hide()
		$Delete.show()
	Globals.fade_in(self)


func _physics_process(delta):
	if plant != null:
		_update_timers_value()
		_update_bars_label()
	else:
		_on_Plant_has_died()


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if not get_global_rect().has_point(event.position) and not _has_dependent_menus_showing():
			Globals.play_sound("close")
			Globals.fade_out(self)


func init(plant, spot):
	self.plant = plant
	self.spot = spot


func _fill_info():
	$Info/NameBox/Name.text = plant.nickname
	$Info/Level.text = tr("LEVEL_LABEL") + ": " + str(plant.level)
	$Info/Production/Label.text = tr("PROD_LABEL") + "/h: " + str(plant.production)
	$Info/Species.text = tr(plant.species)
	_setup_exp_bar()
	_setup_time_bars()


func _update_timers_value():
	var water_normal = $SecondColumn/Bars/Water/Normal
	var water_critic = $SecondColumn/Bars/Water/Critic
	var compost_normal = $SecondColumn/Bars/Compost/Normal
	var compost_critic = $SecondColumn/Bars/Compost/Critic
	water_normal.value = plant.get_timer_time_left("WaterNormal")
	water_critic.value = plant.get_timer_time_left("WaterCritic")
	compost_normal.value = plant.get_timer_time_left("CompostNormal")
	compost_critic.value = plant.get_timer_time_left("CompostCritic")


func _update_bars_label():
	var w_normal = $SecondColumn/Bars/Water/Normal/Label
	var w_critic = $SecondColumn/Bars/Water/Critic/Label
	var c_normal = $SecondColumn/Bars/Compost/Normal/Label
	var c_critic = $SecondColumn/Bars/Compost/Critic/Label
	var exp_bar = $Info/Experience/Label
	w_normal.text = _from_secs_to_hour_str(plant.get_timer_time_left("WaterNormal"))
	w_critic.text = _from_secs_to_hour_str(plant.get_timer_time_left("WaterCritic"))
	c_normal.text = _from_secs_to_hour_str(plant.get_timer_time_left("CompostNormal"))
	c_critic.text = _from_secs_to_hour_str(plant.get_timer_time_left("CompostCritic"))
	exp_bar.text = str(plant.exp_points) + " / " + str(plant.EXP_POINTS_BY_LEVEL[plant.level + 1])
	# critic labels
	w_critic.visible = not plant.water_timer_critic.is_stopped()
	c_critic.visible = not plant.compost_timer_critic.is_stopped()
	# normal labels
	c_normal.visible = not plant.compost_timer_normal.is_stopped()
	w_normal.visible = not plant.water_timer_normal.is_stopped()


func _from_secs_to_hour_str(secs):
	var minutes = int(secs / 60)
	var hours = floor(minutes / 60)
	var min_remainder = int(minutes % 60)
	return ("%02d" % hours) + ":" + ("%02d" % min_remainder) + "h"


func _setup_exp_bar():
	var exp_bar = $Info/Experience
	if plant.level != 10:
		exp_bar.max_value = plant.EXP_POINTS_BY_LEVEL[plant.level + 1]
		exp_bar.value = plant.exp_points
	else:
		exp_bar.hide()


func _setup_time_bars():
	var water_normal = $SecondColumn/Bars/Water/Normal
	var water_critic = $SecondColumn/Bars/Water/Critic
	var compost_normal = $SecondColumn/Bars/Compost/Normal
	var compost_critic = $SecondColumn/Bars/Compost/Critic
	water_normal.max_value = plant.WATER_NORMAL_TIME
	water_critic.max_value = plant.WATER_CRITIC_TIME
	compost_normal.max_value = plant.COMPOST_NORMAL_TIME
	compost_critic.max_value = plant.COMPOST_CRITIC_TIME


func _has_dependent_menus_showing():
	var res = false
	for i in dependent_menus:
		for node in UI.get_children():
			if node.name.find(i) != -1:
				res = true
				break
	return res 


func _on_Plant_exp_points_changed():
	_setup_exp_bar()
	$Info/Level.text = tr("LEVEL_LABEL") + ": " + str(plant.level)
	$Info/Production/Label.text = tr("PROD_LABEL") + "/h: " + str(plant.production)


func _on_Plant_nickname_changed():
	$Info/NameBox/Name.text = plant.nickname


func _on_Plant_has_died():
	$Info.hide()
	$SecondColumn.hide()
	$Delete.show()


func _on_WaterButton_pressed():
	Globals.play_sound("click")
	plant.apply_water()


func _on_CompostButton_pressed():
	Globals.play_sound("click")
	var compost_menu = load("res://scenes/ui/CompostMenu.tscn").instance()
	compost_menu.init(plant)
	UI.add_child(compost_menu)


func _on_EditName_pressed():
	Globals.play_sound("click")
	var edit_name_menu = load("res://scenes/ui/NameEditMenu.tscn").instance()
	edit_name_menu.init(plant)
	UI.add_child(edit_name_menu)


func _on_Misc_pressed():
	Globals.play_sound("click")
	var misc_menu = load("res://scenes/ui/MiscPlantMenu.tscn").instance()
	misc_menu.init(plant, spot)
	UI.add_child(misc_menu)


func _on_Delete_pressed():
	Globals.play_sound("click")
	Globals.owned_pots[plant.current_pot] += 1
	# rip
	set_physics_process(false)
	plant.queue_free()
	Globals.fade_out(self)


func _on_Talk_pressed():
	Globals.play_sound("click")
	var menu = load("res://scenes/ui/TalkMenu.tscn").instance()
	menu.init(plant)
	UI.add_child(menu)
