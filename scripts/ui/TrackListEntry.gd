extends Control


var list_page
var track_name
var shop_mode


func _ready():
	$Container/Name.text = track_name
	$Container/Price/Label.text = str(Globals.TRACKS_PRICES[track_name])
	if not shop_mode:
		$Container/Price.hide()


func _physics_process(delta):
	if shop_mode:
		if Globals.sollars < Globals.TRACKS_PRICES[track_name]:
			$BrownButtonPanel.make_inactive()
		else:
			$BrownButtonPanel.make_active()


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if get_global_rect().has_point(event.position):
			if shop_mode:
				if Globals.sollars >= Globals.TRACKS_PRICES[track_name]:
					Globals.play_sound("purchase")
					var track_index = Globals.tracks_to_buy.find(track_name)
					Globals.tracks_to_buy.remove(track_index)
					Globals.owned_tracks.append(track_name)
					Globals.sollars -= Globals.TRACKS_PRICES[track_name]
					get_node("/root/Game/GameUI/ShopMenu").reload_current_page()
					get_node("/root/Game/GameUI").show_confirm_anim(event.position)
				else:
					# not enough money
					Globals.play_sound("error")
			else:
				Globals.play_sound("click")
				Globals.play_music(track_name)
				Globals.fade_out(get_parent().get_parent())


func init(track_name, list_page, shop_mode):
	self.track_name = track_name
	self.list_page = list_page
	self.shop_mode = shop_mode