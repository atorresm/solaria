extends Control

var plant
var plant_to_sustitute
var spot
var list_page


func _ready():
	$Container/Icon.texture = load("res://assets/ui/plants/" + plant.species.to_lower() 
									+ "_" + "phase" + str(plant.phase) + ".png")


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if get_global_rect().has_point(event.position):
			# if spot already has plant, move that plant to Inactives
			if plant_to_sustitute != null:
				spot.remove_child(plant_to_sustitute)
				Globals.get_node("InactivePlants").add_child(plant_to_sustitute)
			# add plant to spot
			Globals.play_sound("click")
			Globals.get_node("InactivePlants").remove_child(plant)
			spot.add_child(plant)
			# quit the plant selection screen
			Globals.fade_out(get_parent().get_parent())


func init(plant, spot, list_page, plant_to_sustitute = null):
	self.plant = plant
	self.spot = spot
	self.list_page = list_page
	self.plant_to_sustitute = plant_to_sustitute
	# TODO set icon
	$Container/SecondColumn/Species.text = plant.species
	$Container/SecondColumn/Nickname.text = plant.nickname
	$Container/ThirdColumn/Level.text = tr("LEVEL_LABEL") + str(plant.level)
	if plant.level == 10:
		$Container/ThirdColumn/Experience.text = "max"
	else:
		$Container/ThirdColumn/Experience.text = str(plant.exp_points) + " / " + str(plant.EXP_POINTS_BY_LEVEL[plant.level + 1])