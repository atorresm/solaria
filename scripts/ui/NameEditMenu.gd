extends Control


onready var line_edit = $Container/LineEdit
onready var ok_button = $Container/OKButton
var plant = null


func _ready():
	Globals.fade_in(self)
	line_edit.grab_focus()


func _physics_process(delta):
	ok_button.disabled = line_edit.text.length() == 0


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if not get_global_rect().has_point(event.position):
			if OS.get_name() == "Android":
				OS.hide_virtual_keyboard()
			Globals.play_sound("close")
			Globals.fade_out(self)


func init(plant):
	self.plant = plant


func _on_OKButton_pressed():
	plant.nickname = line_edit.text
	if OS.get_name() == "Android":
		OS.hide_virtual_keyboard()
	Globals.play_sound("click")
	Globals.fade_out(self)