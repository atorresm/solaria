extends Control

var unlockables = {"TRACK":[], "STATUE":[], "SEED":[], "POT":[]}
var unlocked_object_type = null
var unlocked_object = null


func _ready():
	_select_item_to_unlock()
	_fill_info()
	Globals.fade_in(self)


func _input(event):
	if event is InputEventScreenTouch and event.is_pressed():
		if not get_rect().has_point(event.position):
			Globals.play_sound("close")
			Globals.fade_out(self)


func _select_item_to_unlock():
	# tracks
	for i in Globals.tracks.keys():
		if not i in Globals.tracks_to_buy and not i in Globals.owned_tracks:
			unlockables["TRACK"].append(i)
	# statues
	for i in Globals.statues:
		if not i in Globals.statues_to_buy and not i in Globals.owned_statues:
			unlockables["STATUE"].append(i)
	# seeds
	for i in Globals.species:
		if not i in Globals.seeds_to_buy:
			unlockables["SEED"].append(i)
	# pots
	for i in Globals.pots:
		if not i in Globals.pots_to_buy:
			unlockables["POT"].append(i)
	# check if available items
	for key in unlockables.keys():
		if unlockables[key].size() == 0:
			unlockables.erase(key)
	# no items to unlock
	if unlockables.keys().size() == 0:
		$AllUnlocked.show()
		return
	unlocked_object_type = unlockables.keys()[randi() % unlockables.keys().size()]
	unlocked_object = unlockables[unlocked_object_type][randi() % unlockables[unlocked_object_type].size()]
	# add unlocked object
	match unlocked_object_type:
		"TRACK":
			Globals.tracks_to_buy.append(unlocked_object)
		"STATUE":
			Globals.statues_to_buy.append(unlocked_object)
		"SEED":
			Globals.seeds_to_buy.append(unlocked_object)
		"POT":
			Globals.pots_to_buy.append(unlocked_object)


func _fill_info():
	match unlocked_object_type:
		"TRACK":
			$Container/CenterContainer/Icon.texture = load("res://assets/ui/music_icon_big.png")
		"STATUE":
			$Container/CenterContainer/Icon.texture = load("res://assets/ui/statue_icon_big.png")
		"SEED":
			$Container/CenterContainer/Icon.texture = load("res://assets/ui/seed_icon_big.png")
		"POT":
			$Container/CenterContainer/Icon.texture = load("res://assets/ui/pot_icon_big.png")
	if unlocked_object != null:
		$Container/Text.text = tr("UNLOCKED_" + unlocked_object_type + "_LABEL") + ":\n" \
						+ tr(unlocked_object)
	else:
		$Container/Text.text = tr("NO_MORE_UNLOCKABLES_LABEL")


func _on_Accept_pressed():
	Globals.play_sound("close")
	Globals.fade_out(self)