![Solaria logo](./repo_resources/solaria_big_logo.png)

## Index
- [About](#about)
- [Features](#features)
- [Screenshots](#screenshots)
- [Download](#download)
- [Building](#building)
- [License](#license)

## About
Participation for the [XII Concurso Universitario de Software Libre](https://concursosoftwarelibre.org/1718/).
You can check the dev blog [here](https://solariagame.wordpress.com/).

Solaria is an Android game that consists in maintaining a garden in a relaxing
ambient.

## Features

* Customize your garden with various plants and decorations.
* Maintain your garden doing common gardening tasks.
* Unlock music themes, decorations and new plants by leveling up your garden.
* Game in real time: day/night cycles with different ambients.

## Screenshots

![Menu screenshot](./repo_resources/menu.png)
![Talk screenshot](./repo_resources/talk.png)
![Shop screenshot](./repo_resources/shop.png)

## Download

**Current version:** 1.0

Downloads for Android and PC (GNU/Linux, Windows, Mac) available [here](https://papaya-games.itch.io/solaria).

## Building

Made in Godot Engine 3.1. Clone the repo, open it with Godot and [export it for Android.](https://docs.godotengine.org/en/3.1/getting_started/workflow/export/exporting_for_android.html)

## License

>This program is Free Software: You can use, study share and improve it at your will. Specifically you can redistribute and/or modify it under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl.html) as published by the Free Software Foundation, version 3.

>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See LICENSE file for the full text of the license.




